#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Generate 10 random numbers between 0 and 10000000.
// You can specify how many numbers to generate on the
// command line (instead of 10).

int main(int argc, char *argv[])
{
    int count = 10;
    if (argc > 1)
    {
        sscanf(argv[1], "%d", &count);
    }
    srandom(time(NULL));
    while(count)
    {
        int r = random() % 10000000;
        printf("%d\n", r);
        count--;
    }
}