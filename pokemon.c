// Keep track of Pokemon
//   Name
//   Hitpoints
//   Type (0=flying, 1=water, 2=grass, 3=ground, 4=fire)

#include <stdio.h>
#include <string.h>

#define MAX 10

struct pokemon
{
	char name[20];
	int hp;
	int type;
};

void printPokemon(struct pokemon p);

int main()
{
	struct pokemon pokedex[MAX];
	
	// Get input for all the Pokemon
	int count = 0;
	for (int i = 0; i < MAX; i++)
	{
		printf("Name of Pokemon %d: ", i);
		scanf("%s", pokedex[i].name);
		if (strcmp(pokedex[i].name, "DONE") == 0) break;
		printf("Hitpoints: ");
		scanf("%d", &pokedex[i].hp);
		printf("Type: ");
		scanf("%d", &pokedex[i].type);
		count++;
	}
	
	printf("----- Pokedex -----\n");
	for (int i = 0; i < count; i++)
	{
		printPokemon(pokedex[i]);
	}
	
	printf("---- Search ----\n");
	printf("Name of Pokemon to find: ");
	char find[20];
	scanf("%s", find);
	for (int i = 0; i < count; i++)
	{
	    if (strcmp(find, pokedex[i].name) == 0)
	    {
	        printPokemon(pokedex[i]);
	        break;
	    }
	}
}

void printPokemon(struct pokemon p)
{
	printf("%s %d %d\n", p.name, p.hp, p.type);
}