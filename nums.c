#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Must supply a filename to read from\n");
        exit(1);
    }
    FILE *in = fopen(argv[1], "r");
    if (!in)
    {
        perror("Can't open file");
        exit(1);
    }
    
    int arrayLength = 10;
    printf("Allocating %d entries\n", arrayLength);
    int *arr = malloc(arrayLength * sizeof(int));
    int entries = 0;
    char line[20];
    while(fgets(line, 20, in) != NULL)
    {
        int num;
        sscanf(line, "%d", &num);
        
        if (entries == arrayLength)
        {
            // Array is full. Make it bigger.
            arrayLength += 10;
            printf("Reallocating space for %d entries\n", arrayLength);
            arr = realloc(arr, arrayLength * sizeof(int));
        }
        arr[entries] = num;
        entries++;
    }
    
    for (int i = 0; i < entries; i++)
    {
        printf("%d %d\n", i, arr[i]);
    }
    
    free(arr);
}