#include <stdio.h>
#include <stdlib.h>

float ** loadGPS(int *size)
{
    // Array of float pointers
    int arrayLength = 4;
    float ** arr = malloc(arrayLength * sizeof(float *));
    
    FILE *in = fopen("gps.txt", "r");
    if (!in)
    {
        perror("Can't open gps file");
        exit(1);
    }
    
    char line[50];
    int entries = 0;
    while (fgets(line, 50, in) != NULL)
    {
        if (entries == arrayLength)
        {
            arrayLength += 4;
            printf("Extending to %d entries\n", arrayLength);
            arr = realloc(arr, arrayLength * sizeof(float *));
        }
        
        float *gps1 = malloc(2 * sizeof(float));
        
        float lat, lon;
        sscanf(line, "%f%f", &lat, &lon);
        gps1[0] = lat;
        gps1[1] = lon;
        
        arr[entries] = gps1;
        entries++;
    }
    
    *size = entries;
    
    return arr;
}


int main()
{
    int length;
    float ** coords = loadGPS(&length);
    
    for (int i = 0; i < length; i++)
    {
        float *gps = coords[i];
        printf("%d: %f %f\n", i, gps[0], gps[1]);
    }
}